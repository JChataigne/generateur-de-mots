import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TableauFreqLettres {
	BufferedReader in;
	
	double [][] tabFreq;
	long[] nbLettresApres;
	long[] nbLettres;

	String[] alphabet;

	public TableauFreqLettres(String filename) {
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
		String line;
		String alphab=" ";
		boolean nouvelleLettre;
		try {
			String lettre;
			while((line = in.readLine()) != null)
			{
				for(int i = 0 ; i < line.length()-1 ; i++){
					lettre=line.substring(i,i+1);
					//recherche si lettre est déjà dans alphabet
					nouvelleLettre=true;
					for(int j = 0 ; j<alphab.length() ; j++){
						if(lettre == alphab.substring(j, j+1)){
							nouvelleLettre = false;
						}
					}
					if(nouvelleLettre) alphab = alphab.concat(lettre);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//on transforme le String alphab en tableau alphabet
		alphabet = new String[alphab.length()];
		for(int i = 0 ; i<alphab.length() ; i++){
			alphabet[i]=alphab.substring(i, i+1);
		}
		
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		tabFreq=new double[alphabet.length][alphabet.length];
		nbLettresApres= new long[alphabet.length];
		nbLettres= new long[alphabet.length];
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		try {
			while((line = in.readLine()) != null)
			{
				line=line.concat(alphabet[0]); 
				line.toLowerCase(); 
				ArrayList<Integer> sequenceIndex=new ArrayList<Integer>();
				for(int i = 0;i<line.length();i++){
					for(int j = 0;j<alphabet.length;j++){
						if(alphabet[j].contains(line.subSequence(i, i+1))){
							if(alphabet[j].length()>1){
								try{
									String sequence=line.substring(i, i+alphabet[j].length());
									if(sequence.equalsIgnoreCase(alphabet[j])){
										sequenceIndex.add(j);
										i+=alphabet[j].length()-1;
										j=alphabet.length;
									}
								}catch(IndexOutOfBoundsException e){
									// si i+alphabet[j].length est trop grand, c que le charactere n'y est pas 
									// => on passe

								}


							}else{
								sequenceIndex.add(j);
								j=alphabet.length;// on sort si on a trouvé un char correspondant
							}
						}
					}
				}

				// on a le mot transformé vers le nouvel alphabet.
				// On peut maintenant commencer le dénombrement

				for(int i = 0;i<sequenceIndex.size();i++){
					if(i==0){ // cas en début de mot
						tabFreq[sequenceIndex.get(i)][0]+=1;
						nbLettresApres[0]+=1;
						nbLettres[sequenceIndex.get(i)]+=1;
					}else{
						tabFreq[sequenceIndex.get(i)][sequenceIndex.get(i-1)]+=1;
						nbLettresApres[sequenceIndex.get(i-1)]+=1;
						nbLettres[sequenceIndex.get(i)]+=1;
					}
				}
			}
			
			for(int i = 0;i<alphabet.length;i++){
				for(int j = 0;j<alphabet.length;j++){

					if(nbLettresApres[j]!=0){
						tabFreq[i][j]/=nbLettresApres[j];
					}else{
						tabFreq[i][j]=0;
					}
				}
			}
			for(int i = 1;i<alphabet.length;i++){
				for(int j = 0;j<alphabet.length;j++){
					tabFreq[i][j]+=tabFreq[i-1][j];
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void printPython() {
		System.out.print("[  ");
		for (int x = 0; x < alphabet.length; x++) {
			if (x == alphabet.length - 1) {
				System.out.print(alphabet[x]+"]"+"\n");
			} else {
				if(alphabet[x].length()>1){
					System.out.print(alphabet[x] + ",");
				}else{
					System.out.print(alphabet[x] + " ,");
				}
			}

		}
		System.out.print("[");
		for (int x = 0; x < alphabet.length; x++) {
			for (int y = 0; y < alphabet.length; y++) {
				if (y == 0) {
					System.out.print("["+alphabet[x]+",");
				}

				if (y == alphabet.length - 1) {
					System.out.print(tabFreq[x][y]);
				} else {
					System.out.print(tabFreq[x][y] + ",");
				}

			}
			if (x == alphabet.length - 1) {
				System.out.print(",nb_"+nbLettres[x]);
				System.out.print("]]\n");

				System.out.print("[");
				for(int i = 0;i<alphabet.length;i++){
					if (i != alphabet.length - 1) {
						System.out.print("nb_"+nbLettresApres[i]+",");
					}else{
						System.out.println("nb_"+nbLettresApres[i]+"]");
					}
				}
			} else {
				System.out.print(",nb_"+nbLettres[x]);
				System.out.print("],\n");
			}

		}

	}
	
	public double[][] getTabFreq(){
		return this.tabFreq;
	}
	public String[] getAlphabet(){
		return this.alphabet;
	}
}

