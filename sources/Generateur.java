import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Generateur extends JFrame implements ActionListener {
	private String []caracteres;
	private double[][] frequences;
	private JLabel label;
	private JButton bouton;
	
	public  Generateur(String caracteres[], double frequences[][]){
		this.caracteres=caracteres;
		this.frequences=frequences;
		this.label = new JLabel("placeholder");
		this.bouton = new JButton("Générer");
		
		this.setSize(400, 300);
		this.setLocationRelativeTo(null);
		this.setTitle("Générateur de mots");
		this.setBackground(Color.GRAY);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container contenu = this.getContentPane();
		contenu.setLayout(new BorderLayout());
		JPanel panN = new JPanel() ;
		contenu.add(panN, "North");
		JPanel panS = new JPanel();
		contenu.add(panS, "South");
		panN.add(bouton);
		panS.add(label);
		bouton.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		label.setText( genMot() );
	}
	
	public String genMot(){
		String mot="";
		int n = caracteres.length;
		boolean b = true;
		String c = caracteres[0];
		int k=0; double proba;
		while(b){
			for(int i=0 ; i<n ; i++){
				if (caracteres[i]==c)
					k=i;
			}
			proba=Math.random();
			for(int i=0 ; i<n ; i++){
				if(proba <= frequences[i][k]){
					c=caracteres[i];
					i+=n;
				}
			}

			mot=mot.concat(c);
			if(c==caracteres[0]) b=false;
		}
		mot=mot.substring(0, mot.length()-1);
		return mot;
	}
	
}
