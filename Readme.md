english translation below

[fr]

Ce programme Java a pour but de générer des mots "ressemblant" à une langue donnée à partir un fichier d'échantillons de cette langue.

## Organisation des fichiers

Le dossier **echantillons** contient des fichiers textes contenant des échantillons (de français, en l'occurence, mais on peut y ajouter d'autres langues). Ces fichiers doivent être placés à la racine du projet. Le fichier *liste_francais* contient une longue liste de 22740 mots français trouvés sur https://www.freelang.com/dictionnaire/dic-francais.php, et *liste_francais_rapide* en contient une version raccourcie. La version raccourcie permet plus de rapidité lors de l'exécution. Le choix du fichier à utiliser se fait dans les sources, dans *Main.java*.

Le dossier **sources** contient le code source du programme. *Main.java* contient la fonction *main*, et c'est là que le fichier d'échantillons à utiliser peut être modifié. *TableauFreqLettres.java* sert à générer le tableau de probabilités associé aux échantillons. Enfin *Generateur.java* dérive de JFrame. Cette classe produit la fenêtre servant d'interface et la fonction qui génère les mots à partir du tableau de probabilités.

## Principe de fonctionnement
Les échantillons de la langue choisie sont analysés pour en tirer un tableau de probabilités. Ce tableau contient, pour chaque lettre de l'alphabet de la langue d'origine présente dans les échantillons, la probabilité de suivre chacune des autres lettres.
A partir de ce tableau, on peut ensuite générer un mot. A partir d'une lettre initiale, on peut choisir la lettre suivante selon la probabilité de chacune de suivre la lettre initiale. On répète l'opération sur la seconde lettre et ainsi de suite jusqu'à ce que le pseudo-caractère "fin de mot" soit obtenu.
Cette méthode est simpliste et tous les mots produits ne sont pas satisfaisants. Un fichier contenant plus d'échantillons donnera de meilleurs résultats mais demande plus de temps pour être analysé. On peut également générer des mots pour une langue fictive à partir d'un tableau de probabilités ne correspondant pas à des échantillons.


[en]

The goal of this Java program is to generate words "resembling" those of a language, given samples of words of that language.

## Files organisation

**echantillons** file contains word samples (of French, but one could add other languages as well). These files must be placed at the project's root. The file *liste_francais* contains a list of 22740 French words (found at https://www.freelang.com/dictionnaire/dic-francais.php), and *liste_francais_rapide* contains a shorter version of it, for more speed. The choice of the file to use is done in *Main.java*.

**sources** file contains the source code of the program. *Main.java* contains the main function, this is where the samples file to use can be changed. *TableauFreqLettres.java* generates the probability array associated to the samples. *Generateur.java* extends JFrame, it produce the interface window and the function that generates words based on the probabilities.

## How it works

The samples are analysed to build a probability array. This array contains, for each letter of the alphabet present in the samples, the probability for each letter to follow it.
Then based on this array, we can generate a word. Having chosen a starting letter, we chose the following letter according to its probability to be after the starting letter. We repeat this operation on the second letter, then the third, until we reach an "end of word" pseudo-character.
This method is very simple and the resulting words may not all be satisfying. The more samples are used, the better the results will be. Words of a fictional language could also be generated based on a random probabilities array.

